/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   fillit.h                                         .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: tdautrem <marvin@le-101.fr>                +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2018/10/13 20:39:11 by tdautrem     #+#   ##    ##    #+#       */
/*   Updated: 2018/10/14 08:33:12 by tdautrem    ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#ifndef FILLIT_H
# define FILLIT_H

# include <fcntl.h>
# include "libft/includes/libft.h"

typedef struct	s_data
{
	int				a;
	int				linebool;
	int				error;
}				t_data;

typedef struct	s_tetris
{
	char			letter;
	char			**piece;
	struct s_tetris	*next;
}				t_tetris;

typedef struct	s_coord
{
	int			x;
	int			y;
}				t_coord;

char			**ft_meminit(char **s, size_t len);
int				ft_construct(char *line, t_tetris *tetris, int a);
char			**ft_tabrep(char **t1, char **t2);
int				ft_freenewpiece(char **na);
int				ft_buildpiece(t_tetris *t, t_coord *min, t_coord *max,
		t_coord *co);

int				ft_freeallcoord(t_coord *co1, t_coord *co2, t_coord *co3);
int				ft_checkvoisin(t_tetris *t, t_coord *co, int psize);
void			ft_changeminmax(int *psize, t_coord **max, t_coord **min,
		t_coord *co);
int				ft_initminmax(int *psize, t_coord **max, t_coord **min,
		t_coord **co);
int				ft_verifpiece(t_tetris *t);

void			ft_puttab(char **tab); //a virer

int				ft_readpiece(int *i, int *linebool, t_tetris *tetris,
		char *line);
int				ft_readn(int *i, int *a, t_tetris **tetris, char *line);
int				ft_readinit(int *i, char *line, t_tetris **tetris, t_data **d);
int				ft_readeach(int *i, t_data **d, t_tetris **tetris, char *line);
int				ft_read(const int fd, t_tetris *tetris);

#endif
