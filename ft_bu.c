/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   ft_jacquie.c                                     .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: tdautrem <marvin@le-101.fr>                +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2018/10/13 20:32:54 by tdautrem     #+#   ##    ##    #+#       */
/*   Updated: 2018/10/14 08:11:54 by tdautrem    ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "fillit.h"

char	**ft_meminit(char **s, size_t len)
{
	int i;

	if (!(s = (char**)malloc(sizeof(char*) * len)))
		return (NULL);
	i = -1;
	while (++i < len)
		s[i] = NULL;
	return (s);
}

int		ft_construct(char *line, t_tetris *tetris, int a)
{
	int i;

	i = -1;
	if (!(tetris->piece[a] = ft_strnew(4)))
		return (1);
	while (line[++i])
	{
		if ((line[i] != '.' && line[i] != '#') || i == 4)
			return (1);
		else
			tetris->piece[a][i] = line[i];
	}
	if (i != 4)
		return (1);
	return (0);
}

char	**ft_tabrep(char **t1, char **t2)
{
	int y;

	y = -1;
	while (t1[++y])
		ft_strdel(&(t1[y]));
	free(t1);
	t1 = NULL;
	return (t2);
}

int		ft_freenewpiece(char **na)
{
	int i;

	i = -1;
	while (na[++i])
		ft_strdel(&(na[i]));
	if (na)
		ft_memdel((void*)&na);
	return (1);
}

int		ft_buildpiece(t_tetris *t, t_coord *min, t_coord *max, t_coord *co)
{
	char **na;

	if (!(na = ft_meminit(na, max->y - min->y + 2)))
		return (1);
	co->y = -1;
	while (++(co->y) + min->y <= max->y)
	{
		co->x = -1;
		na[co->y] = NULL;
		if (!(na[co->y] = ft_strnew(max->x - min->x + 1)))
			return (ft_freenewpiece(na));
		while (++(co->x) + min->x <= max->x)
			na[co->y][co->x] = t->piece[co->y + min->y][co->x + min->x];
	}
	t->piece = ft_tabrep(t->piece, na);
	return (0);
}

int		ft_freeallcoord(t_coord *co1, t_coord *co2, t_coord *co3)
{
	ft_memdel((void*)&co1);
	ft_memdel((void*)&co2);
	ft_memdel((void*)&co3);
	return (1);
}

int		ft_checkvoisin(t_tetris *t, t_coord *co, int psize)
{
	if (!((co->y != 0 && t->piece[co->y - 1][co->x] == '#') ||
				(co->y != 3 && t->piece[co->y + 1][co->x] == '#') ||
				(co->x != 0 && t->piece[co->y][co->x - 1] == '#') ||
				(co->x != 3 && t->piece[co->y][co->x + 1] == '#'))
			|| !(psize < 4))
		return (1);
	return (0);
}

void	ft_changeminmax(int *psize, t_coord **max, t_coord **min, t_coord *co)
{
	(*max)->x = co->x > (*max)->x ? co->x : (*max)->x;
	(*max)->y = co->y > (*max)->y ? co->y : (*max)->y;
	(*min)->x = co->x < (*min)->x ? co->x : (*min)->x;
	(*min)->y = co->y < (*min)->y ? co->y : (*min)->y;
	(*psize)++;
}

int		ft_initminmax(int *psize, t_coord **max, t_coord **min, t_coord **co)
{
	if (!((*co) = (t_coord*)malloc(sizeof(t_coord))) ||
			!((*max) = (t_coord*)malloc(sizeof(t_coord))) ||
			!((*min) = (t_coord*)malloc(sizeof(t_coord))))
		return (1);
	*psize = 0;
	(*co)->y = -1;
	(*max)->x = 0;
	(*max)->y = 0;
	(*min)->x = 3;
	(*min)->y = 3;
	return (0);
}

int		ft_verifpiece(t_tetris *t)
{
	t_coord *co;
	t_coord *max;
	t_coord *min;
	int		psize;

	if (ft_initminmax(&psize, &max, &min, &co))
		return (ft_freeallcoord(co, min, max));
	while (++(co->y) < 4)
	{
		co->x = -1;
		while (++(co->x) < 4)
			if (t->piece[co->y][co->x] == '#')
			{
				if (ft_checkvoisin(t, co, psize))
					return (ft_freeallcoord(co, min, max));
				ft_changeminmax(&psize, &max, &min, co);
			}
	}
	if (psize != 4 || ft_buildpiece(t, min, max, co))
		return (ft_freeallcoord(co, min, max));
	ft_freeallcoord(co, min, max);
	return (0);
}

void	ft_puttab(char **tab) //a ENLEVER
{
	int y;

	y = -1;
	while (tab[++y])
	{
		ft_putstr(tab[y]);
		ft_putstr("\n");
	}
	ft_putstr("-----------\n");
}

int		ft_readpiece(int *i, int *linebool, t_tetris *tetris, char *line)
{
	*linebool = 0;
	if (ft_construct(line, tetris, *i))
		return (1);
	if (*i == 3 && ft_verifpiece(tetris))
		return (1);
	if (*i == 3) // A ENLEVER
		ft_puttab(tetris->piece); // A ENLEVER
	(*i)++;
	return (0);
}

int		ft_readn(int *i, int *a, t_tetris **tetris, char *line)
{
	if (line[0] != '\0')
		return (1);
	if (!((*tetris)->next = (t_tetris*)malloc(sizeof(t_tetris))))
		return (1);
	(*tetris) = (*tetris)->next;
	(*tetris)->next = NULL;
	if (!((*tetris)->piece = ft_meminit((*tetris)->piece, 5)))
		return (1);
	(*tetris)->letter = 'A' + (++(*a));
	*i = 0;
	if (*a == 26)
		return (1);
	return (0);
}

int		ft_readinit(int *i, char *line, t_tetris **tetris, t_data **d)
{
	if (!(*d = (t_data*)malloc(sizeof(t_data))))
		return (1);
	(*d)->error = 0;
	*i = 0;
	(*d)->a = -1;
	if (!((*tetris)->piece = ft_meminit((*tetris)->piece, 5)))
	{
		free(*d);
		return (1);
	}
	(*tetris)->letter = 'A' + (++((*d)->a));
	(*tetris)->next = NULL;
	(*d)->linebool = 1;
	line = NULL;
	return (0);
}

int		ft_readeach(int *i, t_data **d, t_tetris **tetris, char *line)
{
	if (*i < 4)
	{
		if (((*d)->error = ft_readpiece(i, &((*d)->linebool), *tetris, line)))
			return (1);
	}
	else if (*i == 4)
	{
		if (((*d)->error = ft_readn(i, &((*d)->a), tetris, line)))
			return (1);
		(*d)->linebool = 1;
	}
	ft_strdel(&line);
	return (0);
}

int		ft_read(const int fd, t_tetris *tetris)
{
	int		ret;
	char	*line;
	int		i;
	t_data	*d;

	if (ft_readinit(&i, line, &tetris, &d))
		return (1);
	while ((ret = get_next_line(fd, &line, 0)) == 1)
		if (ft_readeach(&i, &d, &tetris, line))
			break;
	ft_strdel(&line);
	get_next_line(fd, &line, 1);
	close(fd);
	if (d->linebool || d->error || i != 4)
	{
		free(d);
		return (1);
	}
	free(d);
	return (0);
}

void	ft_tetrisfree(t_tetris *t)
{
	int a;
	t_tetris *tmp;

	while (t)
	{
		a = -1;
		while (t->piece[++a])
			ft_strdel(&(t->piece[a]));
		ft_memdel((void*)&(t->piece));
		tmp = t;
		t = t->next;
		free(tmp);
		tmp = NULL;
	}
}

int		main(int argc, char **argv)
{
	int		fd;
	t_tetris	*tetris;

	tetris = NULL;
	if (argc == 2 && (fd = open(argv[1], O_RDONLY)) == 3)
	{
		if (!(tetris = (t_tetris*)malloc(sizeof(t_tetris))) || ft_read(fd, tetris))
			ft_putstr("error\n");
		else
			ft_putstr("work\n");
		ft_tetrisfree(tetris);
	}
	else if (argc == 1)
		write(2, "File name missing.\n", 19);
	else
		write(2, "Too many arguments.\n", 20);
	return (0);
}
