/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   ft_jacquie.c                                     .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: tdautrem <marvin@le-101.fr>                +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2018/10/13 20:32:54 by tdautrem     #+#   ##    ##    #+#       */
/*   Updated: 2018/10/14 08:33:30 by tdautrem    ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "fillit.h"

void	ft_tetrisfree(t_tetris *t)
{
	int a;
	t_tetris *tmp;

	while (t)
	{
		a = -1;
		while (t->piece[++a])
			ft_strdel(&(t->piece[a]));
		ft_memdel((void*)&(t->piece));
		tmp = t;
		t = t->next;
		free(tmp);
		tmp = NULL;
	}
}

int		main(int argc, char **argv)
{
	int		fd;
	t_tetris	*tetris;

	tetris = NULL;
	if (argc == 2 && (fd = open(argv[1], O_RDONLY)) == 3)
	{
		if (!(tetris = (t_tetris*)malloc(sizeof(t_tetris))) || ft_read(fd, tetris))
			ft_putstr("error\n");
		else
			ft_putstr("work\n");
		ft_tetrisfree(tetris);
	}
	else if (argc == 1)
		write(2, "File name missing.\n", 19);
	else
		write(2, "Too many arguments.\n", 20);
	return (0);
}
